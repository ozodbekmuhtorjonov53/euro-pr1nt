import React from 'react'
import { f1, f2, f3 } from '../image'

const Footer1 = () => {
  return (
    <div className=' container mx-auto max-w-screen-2xl'>
        <h1 className=' pt-[127px] font-Lato font-bold text-center text-[46px]'>Bizning jamoa</h1>
        {/* {page1} */}
       <div className=' flex-wrap px-[30px] md:px-[160px] mt-16 flex justify-between'>
       <div className='  w-[350px] h-[256px] relative'>
          <div className=' flex justify-center'>
            <img className=' absolute top-0' src={f1} alt="image1" />
          </div>
          <div className='wedu bg-[#FFFDF7] py-[93px] mt-20'>
            <h2 className=' text-center font-bold'>Brandon Richards</h2>
            <p className=' text-center text-[#F26522]'>Fotograf, Bosh dizayner</p>
          </div>
        </div>
       {/* {page2} */}
        <div className='mt-[140px]  md:mt-0 w-[350px] md:h-[256px]  relative'>
          <div className=' flex justify-center'>
            <img className=' absolute top-0' src={f2} alt="image2" />
          </div>
          <div className='wedu1 bg-[#FFFDF7] py-20 mt-20'>
            <h2 className=' text-center font-bold'>Beth Mckinney</h2>
            <p className=' text-center text-[#F26522] '>Mizlar bilan ishlash <br/>
            bo’lim boshlig’i</p>
          </div>
        </div>
        {/* {page3} */}
        <div className='mt-20  md:mt-0  w-[350px] h-[256px]  relative'>
          <div className=' flex justify-center'>
            <img className=' absolute top-0' src={f3} alt="image3" />
          </div>
          <div className='wedu2 bg-[#FFFDF7] py-20 mt-20'>
            <h2 className=' text-center font-bold'>Dianne Fisher</h2>
            <p className=' text-center text-[#F26522] '>Mizlar bilan ishlash <br/>
            bo’lim boshlig’i</p>
          </div>
        </div>
       </div>
       
    </div>
  )
}

export default Footer1
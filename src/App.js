import Footer1 from "./components/Footer1";
import Footer2 from "./components/Footer2";
import Header from "./components/Header";
import Header1 from "./components/Header1";
import Header2 from "./components/Header2";
import NavBar from "./components/NavBar";

function App() {
  return (
    <div className=" container max-w-screen-2xl mx-auto  bg-white">
      <NavBar />
      <Header />
      <Header1/>
      <Header2/>
      <Footer1/>
      <Footer2/>
    </div>
  )

}
      

export default App;

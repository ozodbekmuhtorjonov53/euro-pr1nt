import React from 'react'

const Footer2 = () => {
  return (
    <div className=' mt-[215px] bg-[#FFFDF6]'>
       <div className='md:px-[480px] pt-[66px] px-[100px] pb-10 text-center md:text-start md:flex  md:justify-between'>
       <div className=' leading-10'>
          <h1 className=' font-bold py-2'>Kompaniya</h1>
          <p>Bir haqimizda</p>
          <p>Mahsulotlar</p>
          <p>Xizmatlar</p>
          <p>Bizning mijozlar</p>
          <p>Jamoa</p>
        </div>
       {/* {} */}
       <div className=' leading-10 md:pb-20'>
          <h1 className=' font-bold py-2'>Xizmatlar</h1>
          <p>Ipakli bosma</p>
          <p>Ofset bosma</p>
          <p>Kesish</p>
          <p>Ultra-binafsha rang</p>
          <p>Bo’rttirish</p>
          <p>Laminatsiya</p>
          <p>Qog’oz kashirovka</p>
          <p>O’yib olish</p>
        </div>
        {/* {} */}
        <div className=' leading-10'>
          <h1 className=' font-bold py-2'>Mijozlar uchun</h1>
          <p>Rahbariyat</p>
          <p>Tarix</p>
          <p>Yangiliklar</p>
          <p>Maketlar uchun talab</p>
          <p>Aloqa</p>
        </div>
       </div>
    </div>
  )
}

export default Footer2
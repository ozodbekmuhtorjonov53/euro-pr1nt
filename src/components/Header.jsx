import React from "react";
import { image1, image2 } from "../image";

const Header = () => {
  return (
    <div className=" container mx-auto max-w-screen-2xl md:pl-40 md:flex justify-between">
      <div className="py-9 md:py-40">
        <p className=" text-[23px] px-[30px] md:px-0 text-center md:text-start md:text-[44px] font-Lato md:w-[590px]">
          Nega mijozlar bizni tanlashadi? So'ngi texnologiyalar sifati,{" "}
          <span className=" font-bold">hoziroq buyurtma bering!</span>
        </p>
        <div className="flex justify-center md:block">
          <button className=" flex mt-8 items-center text-[20px] text-white font-Lato rounded-full gap-3 bg-[#F26522] py-3 px-7">
            <p>Buyurtma berish</p>
            <img src={image1} alt="image1" />
          </button>
        </div>
      </div>
      <div className="pl-[30px] pt-11 md:pt-24 md:block">
        <img className=" bg-cover" src={image2} alt="image2" />
      </div>
    </div>
  );
};

export default Header;

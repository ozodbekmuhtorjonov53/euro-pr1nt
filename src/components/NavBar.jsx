import React from "react";
import { logo } from "../image";

const NavBar = () => {
  return (
    <div className=" container max-w-screen-2xl mx-auto px-[30px] py-5 md:px-40 md:pt-9">
      <div className=" md:flex items-center justify-between">
        <div className=" flex justify-center md:block">
          <img className="" src={logo} alt="logo" />
        </div>
        <nav>
          <ul className=" hidden md:flex gap-10 font-Lato text-[14px]">
            <li>Biz haqimizda</li>
            <li>Maxsulotlar</li>
            <li>Xizmatlar</li>
            <li>Mijozlar</li>
            <li>Jamoa</li>
            <li>Kontakt</li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default NavBar;

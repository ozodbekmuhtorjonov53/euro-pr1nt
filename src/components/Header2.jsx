import React from 'react'
import { img1, img10, img11, img12, img2, img3, img4, img5, img6, img7, img8, img9 } from '../image';

const Header2 = () => {
  return (
    <div className=" container mx-auto max-w-screen-2xl md:px-40">
      <div>
        <p className="pt-20 md:pt-0 text-center font-Lato font-bold text-[46px]">
          Mahsulotlar
        </p>
      </div>
      <div className=' flex flex-wrap justify-center md:flex md:justify-between pt-10'>
        <img className=' rounded-xl mt-10  bg-cover w-[350px] md:w-[270px] md:block ' src={img1} alt="img1" />
        <img className=' rounded-xl mt-10  bg-cover w-[350px] md:w-[270px] md:block ' src={img2} alt="img1" />
        <img className=' rounded-xl mt-10  bg-cover w-[350px] md:w-[270px] md:block ' src={img3} alt="img1" />
        <img className=' rounded-xl mt-10  bg-cover w-[350px] md:w-[270px] md:block ' src={img4} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img5} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img6} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img7} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img8} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img9} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img10} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img11} alt="img1" />
        <img className=' mt-10 rounded-xl md:block  w-[350px] md:w-[270px] bg-cover ' src={img12} alt="img1" />
      </div>
    </div>
  );
}

export default Header2
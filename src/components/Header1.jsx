import React from 'react'
import { logo1 } from '../image'

const Header1 = () => {
  return (
    <div className=" container mx-auto max-w-screen-2xl flex px-[30px] md:px-40 justify-between">
      <div className="hidden md:block bg-cover md:pt-44">
        <img src={logo1} alt="logo1" />
      </div>
      <div className=" md:pt-56 pt-20">
        <h1 className="text-[35px] text-center md:text-start font-Lato font-bold text-[#131A22] md:text-[40px] md:w-[380px]">
          «EUROPRINT» tipografiyasi haqida
        </h1>
        <p className=" md:w-[600px] pt-7 font-Lato text-[16px] text-[#131A22]">
          "EUROPRINT" o'zbek bosmaxonasi - bu sizning biznesingizga yuqori
          darajadagi professional yondashuv. Bosma mahsulotlarning ahamiyatini
          ortiqcha baholash qiyin. Axir, sizning kompaniyangizning imidji bunga
          bog'liq. Biz, o'zbek matbaa ijodiy uyi, Sizga eng yuqori darajadagi
          bosma dizayn va ishlab chiqarishni rivojlantirish bo'yicha xizmatlarni
          ko'rsatishga tayyormiz. Bizning ishimizda sifat, samaradorlik va
          mijozning vazifalariga individual yondoshishni muvaffaqiyatli
          birlashtiramiz.
          <br />
          <br />
          "EUROPRINT" bosmaxonasi:
        </p>
        <div className=' leading-10 pt-2'>
          <h2 className=' font-Lato font-bold'>Katta tajriba</h2>
          <h2 className=' font-Lato font-bold'>Eng yangi uskunalar</h2>
          <h2 className=' font-Lato font-bold'>Keng doiradagi xazmatlar</h2>
          <h2 className=' font-Lato font-bold'>Buyurtmaning eng tezkor bajarilishi.</h2>
        </div>
      </div>
    </div>
  );
}

export default Header1
